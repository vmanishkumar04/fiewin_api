import 'reflect-metadata'; // We need this in order to use @Decorators

import config from './config';
import helmet from 'helmet';
import express from 'express';
import fs from 'fs';
import path from 'path';
import Logger from './loaders/logger';
import morgan from 'morgan';
async function startServer() {
  const app = express();
  const server = require('ws').Server;
  const socket = new server({ port: 8000 });
  socket.on('connection', function (ws) {
    ws.on('request', () => {
      console.log('hii');
    });
  });
  app.use(
    morgan('combined', {
      stream: fs.createWriteStream(path.join(__dirname, 'access.log'), { flags: 'a' }),
    }),
  );
  app.use(helmet());
  await require('./loaders').default({ expressApp: app });

  app
    .listen(config.port, () => {
      Logger.info(`
      ################################################
      🛡️  Server listening on port: ${config.port} 🛡️
      ################################################
    `);
    })
    .on('error', err => {
      Logger.error(err);
      process.exit(1);
    });
}
startServer();
