import { Router } from 'express';
import auth from './routes/auth';
import agendash from './routes/agendash';
import test from './routes/test';
import Users from './routes/Users';
import Game from './routes/Game';
import Images from './routes/Images';

// guaranteed to get dependencies
export default () => {
  const app = Router();
  auth(app);
  agendash(app);
  test(app);
  Users(app);
  Game(app);
  Images(app);

  return app;
};
