import UserService from '@/services/users';
import { Router, Request, Response } from 'express';
import { Container } from 'typedi';
import { Joi, Segments, celebrate, errors, isCelebrateError } from 'celebrate';
import { getToken } from '../middlewares/user';
const route = Router();

const otpOption = {
  [Segments.BODY]: Joi.object().keys({
    name: Joi.string().required(),
    device_Id: Joi.string().required(),
    mobileNo: Joi.string().required(),
    emailid: Joi.string().required(),
    OTP: Joi.string().required(),
  }),
};
export default (app: Router) => {
  app.use('/v1/users', route);
  app.use((err, req, res, next) => {
    let message = err.message;
    if (isCelebrateError(err)) {
      console.log(err.details.get('body').message);
      if (err.details.get('body')) {
        message = err.details.get('body').message;
        return res.json({ success: false, message });
      }
      if (err.details.get('params')) {
        message = err.details.get('params').message;
        return res.json({ success: false, message });
      }
      if (err.details.get('query')) {
        message = err.details.get('query').message;
        return res.json({ success: false, message });
      }
    } else {
      next();
    }
  });
  //   for OTP
  // request : {
  // "appid": "ROUNDPAYAPPID13APR20191351",
  // "domain": "mlm.roundpay.net",
  // "imei": device imei code|| device id,
  // "regKey": "",
  // "serialNo": device S.R Number || device id,
  // "MobileNo":"1234567890",
  // "version": "1.0"
  // }
  route.post('/getotp', celebrate(otpOption), async (req: Request, res: Response) => {
    let data: { message: string; success: boolean } = await Container.get(UserService).getOtp(req, res);
    return res.json(data).status(200);
  });

  const signOption = {
    [Segments.BODY]: Joi.object().keys({
      name: Joi.string().required(),
      device_Id: Joi.string().required(),
      mobileNo: Joi.string().required(),
      emailid: Joi.string().required(),
      OTP: Joi.string().required(),
    }),
  };
  //   for Front signUp
  // request : {
  //     "appid": "ROUNDPAYAPPID13APR20191351",
  //     "domain": "mlm.roundpay.net",
  //     "imei": "k69v1_64_titan_buffalo",
  //     "regKey": "",
  //     "serialNo": "k69v1_64_titan_buffalo",
  //   "version": "1.0",
  //     "userCreate": {
  //         "GenerateOTP": "",
  //         "OTP": "9388",
  //         "address": "lucknow",
  //         "emailid": "vashu@gmail.com",
  //         "mobileNo": "8736079780",
  //         "name": "Vashu",
  //         "outletName": "",
  //         "pincode": "",
  //         "referalID": "1",
  //         "roleID": 3
  //     }
  // }
  route.post('/signup', celebrate(signOption), getToken, async (req: Request, res: Response) => {
    let data: { message: string; success: boolean } = await Container.get(UserService).signUp(req, res);
    return res.json(data).status(200);
  });

  // login OTP

  // request
  // "appid": "ROUNDPAYAPPID13APR20191351",
  // "domain": "mlm.roundpay.net",
  // "imei": "Y8HFxzsyecvfqzM",
  // "regKey": "",
  // "serialNo": "5b8c58332dad582f",
  // "MobileNo": "7985658041",
  // "Otp":"2481",
  // "version": "1.0"

  const LoginOtpOption = {
    [Segments.BODY]: Joi.object().keys({
      deviceId: Joi.string().required(),
      otp: Joi.string().required(),
      mobileNo: Joi.string().required(),
    }),
  };

  route.post('/getloginotp', celebrate(LoginOtpOption), async (req: Request, res: Response) => {
    let data: { message: string; success: boolean } = await Container.get(UserService).getLoginOtp(req, res);
    return res.json(data).status(200);
  });

  // validate otp
  // request
  //     "appid": "ROUNDPAYAPPID13APR20191351",
  //     "domain": "mlm.roundpay.net",
  //     "imei": "Y8HFxzsyecvfqzM",
  //     "regKey": "",
  //     "serialNo": "5b8c58332dad582f",
  //     "MobileNo": "7985658041",
  //     "Otp":"2481",
  //     "version": "1.0"
  const LoginOption = {
    [Segments.BODY]: Joi.object().keys({
      deviceId: Joi.string().required(),
      otp: Joi.string().required(),
      mobileNo: Joi.string().required(),
    }),
  };

  route.post('/getloginotpvalidate', celebrate(LoginOption), getToken, async (req: Request, res: Response) => {
    let data: { message: string; success: boolean } = await Container.get(UserService).getValidateOtp(req, res);
    return res.json(data).status(200);
  });
};
route.use(errors());
