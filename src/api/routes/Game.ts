import UserService from '@/services/users';
import { Router, Request, Response } from 'express';
import { Container } from 'typedi';
import { Joi, Segments, celebrate, errors, isCelebrateError } from 'celebrate';
import { getToken } from '../middlewares/user';
import GameService from '@/services/game';
const route = Router();

// const otpOption = {
//   [Segments.BODY]: Joi.object().keys({
//     name: Joi.string().required(),
//     device_Id: Joi.string().required(),
//     mobileNo: Joi.string().required(),
//     emailid: Joi.string().required(),
//     OTP: Joi.string().required(),
//   }),
// };
export default (app: Router) => {
  app.use('/v1/game', route);
  app.use((err, req, res, next) => {
    let message = err.message;
    if (isCelebrateError(err)) {
      console.log(err.details.get('body').message);
      if (err.details.get('body')) {
        message = err.details.get('body').message;
        return res.json({ success: false, message });
      }
      if (err.details.get('params')) {
        message = err.details.get('params').message;
        return res.json({ success: false, message });
      }
      if (err.details.get('query')) {
        message = err.details.get('query').message;
        return res.json({ success: false, message });
      }
    } else {
      next();
    }
  });
  //  routes

  route.post('/insertGame', async (req: Request, res: Response) => {
    let data: { message: string; success: boolean; token: any } = await Container.get(GameService).insert(req, res);
    return res.json(data).status(200);
  });

  route.get('/gameList', async (req: Request, res: Response) => {
    let data: { message: string; success: boolean; token: any } = await Container.get(GameService).list(req, res);
    return res.json(data).status(200);
  });
};
route.use(errors());
