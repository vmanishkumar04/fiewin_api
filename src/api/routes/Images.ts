import ImageService from '@/services/images';
import { Router, Request, Response } from 'express';
import { Container } from 'typedi';
import middlewares from '../middlewares';
import multer from 'multer';
const route = Router();

export default (app: Router) => {
  app.use('/v1/image', route);

  // configations
  const storage = multer.diskStorage({
    destination: function (req, file, cb) {
      cb(null, '/tmp/my-uploads');
    },
    filename: function (req, file, cb) {
      const uniqueSuffix = Date.now() + '-' + Math.round(Math.random() * 1e9);
      cb(null, file.fieldname + '-' + uniqueSuffix);
    },
  });
  const fileFilter = function (req, file, cb) {
    if (file.mimetype == 'image/jpg' || file.mimetype == 'image/jpeg' || file.mimetype == 'image/png') {
      cb(null, true);
    } else {
      cb(null, false);
    }
  };
  const upload = multer({
    storage: storage,
    fileFilter: fileFilter,
  });
  // config over

  // upload.single("key")

  route.post('/post', upload.single('img'), async (req: Request, res: Response) => {
    let data: { message: string; success: boolean; token: any } = await Container.get(ImageService).postImage(req, res);
    return res.json(data).status(200);
  });
};
