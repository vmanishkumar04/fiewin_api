import jwt from 'jsonwebtoken';
import config from '@/config';
export const setToken = async data => {
  const token = await jwt.sign(data, config.jwtSecret, { expiresIn: '0.01h' });
  return token;
};

export const getToken = async (req, res, next): Promise<{ message: string; success: boolean; token: any }> => {
  if (
    (req.headers.authorization && req.headers.authorization.split(' ')[0] === 'Token') ||
    (req.headers.authorization && req.headers.authorization.split(' ')[0] === 'Bearer')
  ) {
    let token = req.headers.authorization.split(' ')[1];
    jwt.verify(token, config.jwtSecret, function (err) {
      if (err) {
        return res.status(401).json({ message: 'Token expired', success: false, token: null });
      } else {
        next();
      }
    });
  } else {
    return res.json({ message: 'Token not found', success: false, token: null });
  }
};
