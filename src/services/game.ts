import { Service, Inject } from 'typedi';
import MailerService from './mailer';
import insertGame from '../models/game';
import { EventDispatcher, EventDispatcherInterface } from '@/decorators/eventDispatcher';
import { Request, Response } from 'express';

@Service()
export default class GameService {
  constructor(
    @Inject('userModel') private userModel: Models.UserModel,
    private mailer: MailerService,
    @Inject('logger') private logger,
    @EventDispatcher() private eventDispatcher: EventDispatcherInterface,
  ) {}
  // request
  // {
  // name:"String",
  // img: "String"
  // }
  public async insert(req: Request, res: Response): Promise<{ message: string; success: boolean; token: any }> {
    try {
      const data = await insertGame(req.body);
      await data.save();
      return { message: data, success: true, token: null };
    } catch (e) {
      return { message: e.message, success: false, token: null };
    }
  }

  public async list(req: Request, res: Response): Promise<{ message: string; success: boolean; token: any }> {
    try {
      const data = await insertGame.find({}, { name: 1, img: 1, _id: 0 });
      return { message: data, success: true, token: null };
    } catch (e) {
      return { message: e.message, success: false, token: null };
    }
  }
}
