import { Service, Inject } from 'typedi';
import MailerService from './mailer';
import testUser from '../models/user';
import { EventDispatcher, EventDispatcherInterface } from '@/decorators/eventDispatcher';
import { Request, Response } from 'express';

@Service()
export default class TestService {
  constructor(
    @Inject('userModel') private userModel: Models.UserModel,
    private mailer: MailerService,
    @Inject('logger') private logger,
    @EventDispatcher() private eventDispatcher: EventDispatcherInterface,
  ) {}

  public async hello(req: Request, res: Response): Promise<{ message: string; flag: boolean }> {
    try {
      const dummy = await testUser({
        name: 'Vashu',
        email: 'abc@gmail.com',
      });
      await dummy.save();
      return { message: 'API WORKS', flag: true };
    } catch (e) {
      return { message: e.message, flag: false };
    }
  }
  public async see(req: Request, res: Response): Promise<{ message: string; flag: boolean }> {
    try {
      const dummy = await testUser.find({});
      return { message: dummy, flag: true };
    } catch (e) {
      return { message: e.message, flag: false };
    }
  }
}
