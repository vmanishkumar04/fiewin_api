import { Service, Inject } from 'typedi';
import MailerService from './mailer';
import { EventDispatcher, EventDispatcherInterface } from '@/decorators/eventDispatcher';
import { Request, Response } from 'express';
@Service()
export default class ImageService {
  constructor(
    @Inject('userModel') private userModel: Models.UserModel,
    private mailer: MailerService,
    @Inject('logger') private logger,
    @EventDispatcher() private eventDispatcher: EventDispatcherInterface,
  ) {}

  public async postImage(req: Request, res: Response): Promise<{ message: string; success: boolean; token: any }> {
    try {
      return { message: 'API WORKS', success: true, token: null };
    } catch (e) {
      return { message: e.message, success: false, token: null };
    }
  }
}
