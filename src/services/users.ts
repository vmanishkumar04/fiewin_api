import { Service, Inject, Token } from 'typedi';
import MailerService from './mailer';
import { postResquest } from '../partials/api';
import { EventDispatcher, EventDispatcherInterface } from '@/decorators/eventDispatcher';
import { Request, Response } from 'express';
import config from '@/config';
import { setToken } from '@/api/middlewares/user';
@Service()
export default class UserService {
  constructor(
    @Inject('userModel') private userModel: Models.UserModel,
    private mailer: MailerService,
    @Inject('logger') private logger,
    @EventDispatcher() private eventDispatcher: EventDispatcherInterface,
  ) {}

  public async getOtp(req: Request, res: Response): Promise<{ message: string; success: boolean; token: any }> {
    try {
      const { deviceId, mobileNo } = req.body;
      const data = {
        appid: config.common.appid,
        domain: config.common.domain,
        imei: deviceId,
        regKey: '----',
        serialNo: deviceId,
        MobileNo: mobileNo,
        version: '1.0',
      };
      const res = await postResquest(config.users.otp, data);
      const getToken = await setToken(data);
      return { message: res.data, success: true, token: getToken };
    } catch (e) {
      return { message: e.message, success: false, token: null };
    }
  }

  public async signUp(req: Request, res: Response): Promise<{ message: string; success: boolean }> {
    try {
      const { name, mobileNo, OTP, device_Id, emailid } = req.body;
      const data = {
        appid: config.common.appid,
        domain: config.common.domain,
        imei: device_Id,
        regKey: '----',
        serialNo: device_Id,
        version: '1.0',
        userCreate: {
          GenerateOTP: '----',
          OTP,
          address: '----',
          emailid,
          mobileNo,
          name,
          outletName: '----',
          pincode: '----',
          referalID: '1',
          roleID: 3,
        },
      };
      const res = await postResquest(config.users.signUP, data);
      return { message: res.data, success: true };
    } catch (e) {
      return { message: e.message, success: false };
    }
  }

  //  login
  // otp
  public async getLoginOtp(req: Request, res: Response): Promise<{ message: string; success: boolean; token: any }> {
    try {
      const { deviceId, mobileNo } = req.body;
      const data = {
        appid: config.common.appid,
        domain: config.common.domain,
        imei: deviceId,
        regKey: '----',
        serialNo: deviceId,
        MobileNo: mobileNo,
        version: '1.0',
      };
      const res = await postResquest(config.users.Loginotp, data);
      const getToken = await setToken(data);
      return { message: res?.data, success: true, token: getToken };
    } catch (e) {
      return { message: e.message, success: false, token: null };
    }
  }
  // validate otp
  public async getValidateOtp(req: Request, res: Response): Promise<{ message: string; success: boolean }> {
    try {
      const { deviceId, mobileNo, otp } = req.body;
      const data = {
        appid: config.common.appid,
        domain: config.common.domain,
        imei: deviceId,
        regKey: '----',
        serialNo: deviceId,
        MobileNo: mobileNo,
        Otp: otp,
        version: '1.0',
      };
      const res = await postResquest(config.users.Validateotp, data);
      console.log(res?.data);
      return { message: res.data, success: true };
    } catch (e) {
      return { message: e.message, success: false };
    }
  }
}
