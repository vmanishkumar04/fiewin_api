import { IUser } from '@/interfaces/IUser';
import mongoose from 'mongoose';

const Game = new mongoose.Schema(
  {
    name: {
      type: String,
      index: true,
    },
    img: {
      type: String,
    },
  },
  { timestamps: true },
);

export default mongoose.model<IUser & mongoose.Document>('Game', Game);
